function menuLoad = setup_menuLoad()


menuLoad = uimenu('Label',' Load ');


    uimenu(menuLoad, 'Label',' Select OQ Calculation ',...
                     'Callback','OqCalcInfo = select_oqCalculation();');
    
    uimenu(menuLoad, 'Label',' Load Exposure ',...
                     'Callback','Exposure = get_exposure(OqCalcInfo);');
                 
                 
    uimenu(menuLoad, 'Label',' Load Ruptures ',...
                     'Callback','Ruptures = get_ruptures(OqCalcInfo);');
                 
                 
    uimenu(menuLoad, 'Label',' Load ELT ',...
                     'Callback','ELT = get_elt(OqCalcInfo);');
                 
                 
    uimenu(menuLoad, 'Label',' Load PML ',...
                     'Callback','[pml_detailed,pml_agg] = get_pmlCurve(OqCalcInfo);');
                 
                 
                 