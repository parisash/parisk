function menuPLA = setup_menuPLA()


menuPLA = uimenu('Label',' PLA ');


    uimenu(menuPLA, 'Label',' Get PLA Factor ',...
                    'Callback','plaFactor = create_plaFactor();');
                 
                 
    uimenu(menuPLA, 'Label',' Add PLA Effect ',...
                    'Callback','ELT = add_plaEffect(plaFactor,ELT,Exposure);');
                 
               