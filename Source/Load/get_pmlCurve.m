function [pml_detailed,pml_agg] = get_pmlCurve(OqCalcInfo)

tStart = disp_fctStart();

%%
pml_detailed = readtable(fullfile(OqCalcInfo.oqPath,OqCalcInfo.agg_curves_stats),'HeaderLines',1');

%%
[group, idPeriod,idStat,idLossType] = findgroups(pml_detailed.return_period,pml_detailed.stat,pml_detailed.loss_type);

func    = @(value, ratio, freq) [sum(value), sum(value)./sum(value./ratio), freq(1)];
lossAgg = splitapply(func, ...
                     pml_detailed.loss_value, ...
                     pml_detailed.loss_ratio, ...
                     pml_detailed.annual_frequency_of_exceedence, ...
                     group);


pml_agg = table();

pml_agg.return_period = idPeriod;
pml_agg.stat          = idStat;
pml_agg.loss_type     = idLossType;

pml_agg.loss_value                     = lossAgg(:,1);
pml_agg.loss_ratio                     = lossAgg(:,2);
pml_agg.annual_frequency_of_exceedence = lossAgg(:,3);


disp_fctEnd(tStart);
