function Exposure = get_exposure(OqCalcInfo)

tStart = disp_fctStart();

Exposure.Detailed    = readtable(fullfile(OqCalcInfo.oqPath,OqCalcInfo.exposure_model));

%%
Exposure.County = varfun(@sum,Exposure.Detailed,...
                             'GroupingVariables',{'NAME_1','NAME_2'},...
                             'InputVariables',{'number','structural','nonstructural','area'});

%%
Exposure.Aggregated = varfun(@sum,Exposure.Detailed,...
                                 'GroupingVariables',{'lon','lat'},...
                                 'InputVariables',{'number','structural','nonstructural','area'});
                          
                          
[groupIdx,~] = findgroups(Exposure.Detailed(:,{'lon','lat'}));

Exposure.Aggregated.NAME_1 = splitapply(@unique,Exposure.Detailed.NAME_1,groupIdx);
Exposure.Aggregated.NAME_2 = splitapply(@unique,Exposure.Detailed.NAME_2,groupIdx);
                          
disp_fctEnd(tStart);
