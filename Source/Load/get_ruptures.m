function Ruptures = get_ruptures(OqCalcInfo)

tStart = disp_fctStart();

Ruptures = readtable(fullfile(OqCalcInfo.oqPath,OqCalcInfo.ruptures),'HeaderLines',1);
                          
disp_fctEnd(tStart);