function ELT = get_elt(OqCalcInfo)

tStart = disp_fctStart();

%% load data
% event_id/taxonomy/nonstructural/structural/rlz_id/rup_id/year
losses_by_event = readtable(fullfile(OqCalcInfo.oqPath,OqCalcInfo.losses_by_event),'HeaderLines',1);
ruptures        = readtable(fullfile(OqCalcInfo.oqPath,OqCalcInfo.ruptures),'HeaderLines',1);

%% creat elt 
% aggregate losses per event
% elt: eventID/rup_id/lon/lat/nonstructural/structural

[eventIds,ia,subs] = unique([losses_by_event.event_id losses_by_event.rlz_id],'rows');
rupIds             = losses_by_event.rup_id(ia);
loc_idx            = arrayfun(@(x) find(ruptures.rupid == rupIds(x)), 1:size(eventIds,1));

ELT = table();
ELT.eventID       = eventIds(:,1);
ELT.rupid         = rupIds;
ELT.lon           = ruptures.centroid_lon(loc_idx);
ELT.lat           = ruptures.centroid_lat(loc_idx);
ELT.nonstructural = accumarray(subs,losses_by_event.nonstructural);
ELT.structural    = accumarray(subs,losses_by_event.structural);

%%
disp_fctEnd(tStart);

