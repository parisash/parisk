function OqCalcInfo = select_oqCalculation()

[OqCalcInfo.losses_by_event,...
 OqCalcInfo.oqPath]         = uigetfile('losses_by_event_*.csv');

OqCalcInfo.number_calc      = regexp(OqCalcInfo.losses_by_event,'\d*','Match');
 
OqCalcInfo.exposure_model   = 'exposure_model.csv';
 
OqCalcInfo.ruptures         = ['ruptures_', OqCalcInfo.number_calc{1},'.csv'];
 
OqCalcInfo.agg_curves_stats = ['agg_curves-stats_',OqCalcInfo.number_calc{1},'.csv'];

