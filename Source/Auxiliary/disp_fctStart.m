function tStart = disp_fctStart(layer)

%   tStart = disp_fctStart(layer)
%
%   Create formatted terminal output and write terminal output at function
%   start.
%
%   INPUT
%       layer         : integer specifying the output text indentation
%                       [optional] 0 - add additional newline at the beginning
%
%   OUTPUT
%       tStart        : start time of function (use as input for disp_fctEnd.m)
%

tStart = tic;

%% set layer to 0, if not given as input
if nargin < 1
    layer = 0;
end

%% use additional newline for layer 0
if layer == 0
    indentation = '\n';
else
    indentation = repmat('  ', 1, layer);
end

%% get name of caller function (empty if called from terminal)

stack = dbstack();

if length(stack) < 2
    filenameCaller = '';
else
    filenameCaller = stack(2).name;
end

%% print to terminal

fprintf( [ indentation 'Start of function ' filenameCaller ' ...\n' ] );

