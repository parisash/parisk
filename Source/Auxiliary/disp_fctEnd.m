function disp_fctEnd(tStart,layer)

%   disp_fctEnd(tStart,layer)
%
%   Create formatted terminal output and write terminal output at function end
%
%   INPUT
%    tStart        : double specifying the starting time of the invoking function
%    layer         : integer specifying the output text indentation
%
%   OUTPUT
%

%% Input checks

if nargin < 1
    tEnd = 0;
else
    tEnd = toc(tStart);
end

if nargin < 2
    layer = 0;
end

indentation = repmat( '  ', 1, layer );

%% get name of caller function (empty if called from terminal)

stack = dbstack();

if length(stack) < 2
    filenameCaller = '';
else
    filenameCaller = stack(2).name;
end

%% print to terminal

str   = '%sEnd   of function %s ( runtime: %.1f sec )\n';

% add a second newline for layer 0
if layer==0
    str = [str, '\n'];
end

fprintf( str, indentation, filenameCaller, tEnd );


