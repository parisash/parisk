clc
close all
clear all

disp(' ')
disp('         **  ')
disp('        **** ')
disp('       PARISK')
disp('        **** ')
disp('         **  ')
disp(' ')

%%
setup_mainFig()


function setup_mainFig()

figID_main = figure('Name','PARISK','NumberTitle','off');
set(figID_main,'units','normal','OuterPosition',[0.2 0.2 0.5 0.5]);
title('IEIP','FontSize',14,'Interpreter','none')
hold on


%% load and plot country boundary
% path_to_Boundry = '\\mucdv00060\GC\GeoRisks\Earthquake\User\Students\Shahbazi\PARISK\Data\RL17_Geozones_shp';
% Admin0          = shaperead(fullfile(path_to_Boundry,'IR_ADMIN0_RL17'));
% countryBoundary = [[Admin0(:).X]',[Admin0(:).Y]'];
% plot(countryBoundary(:,1),countryBoundary(:,2),'k','LineWidth',1.5);


% %% load and plot top 10 cities
% path_to_cities = '\\mucdv00060\GC\GeoRisks\Earthquake\User\Students\Shahbazi\PARISK\Data\mat';
% load(fullfile(path_to_cities,'IR_cities.mat'), 'Top10_cities');

% plot_cities(Top10_cities,figID_main);


%% Load
menuLoad = setup_menuLoad();

%% PLA
menuPLA = setup_menuPLA();

%% Plot
menuPlot = setup_menuPlot();

%%
menuExit = uimenu('Label','EXIT',...
                  'Callback',...
                  ['ButtonName = questdlg(''Do you really want to EXIT?'','...
                  ' ''Exit Window'',',...
                  ' ''YES'' , ''NO'', ''NO'');',...
                  'if exist(''ButtonName'',''var'') & strcmp(ButtonName, ''YES'') ;',...
                       'close all, clear, home;',...
                  'end;']);
				  
end