function plotId_Ruptures = plot_ruptures(Ruptures)

ruptureNr = inputdlg('enter rupture number')

radius = 250;

ruptureLocations = unique([Ruptures.centroid_lon,Ruptures.centroid_lat],'rows');

radiusDegree     = radius / 111 ./ sqrt( cosd(ruptureLocations(:,2)).^2 + 1 );

plotId_Ruptures = circle_plot(ruptureLocations(:,1),ruptureLocations(:,2),radiusDegree);


function h = circle_plot(x,y,r)

th = (0: pi/50: 2*pi)';

h = [];

for iPoint = 1:length(x)
    
    hold on
    xunit = x(iPoint) + r(iPoint) * cos(th);
    yunit = y(iPoint) + r(iPoint) * sin(th);

    h = [h,plot(xunit, yunit)];

    hold off
    
end