function [pml,aal,stddev] = get_pmlCurve(ELT)

wkpPoints = logspace(-1,5,1201)';

if size(ELT,1) < 2
    pml    = [wkpPoints,zeros(1201,1)];
    aal    = 0;
    stddev = 0;
    return
end


lossRates  = ELT(:,2);
lossValues = ELT(:,3);


%% compute AAL (Average Annual Loss) and StdDev (Standard Deviation, Assume Poisson Distribution)

aal = sum(lossValues.*lossRates,1);
stddev = sqrt(sum(lossRates.*lossValues.^2,1));


%% compute PML

% create default PML with values zeros on wkpPoints
pml      = zeros(size(wkpPoints,1),2);
pml(:,1) = wkpPoints;

% use only loss data with lossRate > 0
indexLossRate = find(lossRates > 0);

if size(indexLossRate,1) > 0

    % sort loss values with corresponding rates in ascending order
    lossAscending = sortrows([lossValues(indexLossRate),lossRates(indexLossRate)],1);

    % build cumulative sum over loss rates starting from largest loss
    cumLossRates  = cumsum(flipud(lossAscending(:,2)));
    
    % compute cumulative return periods and put them in ascending order
    cumRP = flipud(1./cumLossRates);

    % find nearest loss values of wkpPoints
    pml(:,2) = interp1(cumRP,lossAscending(:,1),wkpPoints,'nearest','extrap');
    
    % set pml for RP < min(cumRP of ELT) to 0
    pml(pml(:,1)<min(cumRP),2) = 0;
    
end

