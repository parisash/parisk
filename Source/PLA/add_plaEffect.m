function ELT = add_plaEffect(plaFactor,ELT,Exposure)

tStart = disp_fctStart();


radius = 250;

nEvents = height(ELT);

%% add circle loss ratio
[groupIdx,eventsLocations] = findgroups(ELT(:,{'lon','lat'}));

distanceAssetToEvent = arrayfun(@(x) sqrt(  ( (Exposure.Aggregated.lon - eventsLocations.lon(x) ) * 111 * cosd(eventsLocations.lat(x)) ).^2 + ...
                                            ( (Exposure.Aggregated.lat - eventsLocations.lat(x) ) * 111                                ).^2 ),...
                                 (1:height(eventsLocations))','uniformOutput',false);

exposureIdx = arrayfun(@(x) find(distanceAssetToEvent{x} <= radius), (1:height(eventsLocations))','uniformOutput',false);

exposureAtCircle = arrayfun(@(x) sum(Exposure.Aggregated.sum_nonstructural(exposureIdx{x})),(1:height(eventsLocations))');

exposureOfEvents = exposureAtCircle(groupIdx);

ELT.nonstructuralEventLossRatio = ELT.nonstructural./exposureOfEvents;

%% add PLA factor
ELT.plaFactor   = plaFactor(ELT.nonstructural,ELT.nonstructuralEventLossRatio);
ELT.lossWithPLA = ELT.plaFactor .* ELT.nonstructural;

%%
figure(100); hold on
plot3(ELT.nonstructural,ELT.nonstructuralEventLossRatio,ELT.plaFactor,'k.')

disp_fctEnd(tStart);

