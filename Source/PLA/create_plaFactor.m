function plaFactor = create_plaFactor()


loss = 0:1e8:8e9;

lossRatio = 0:0.01:0.25;

plaFactor = @(x,y) 1 + ((x./10e9).^0.4) .* (y.^0.6);

[x,y] = meshgrid(loss, lossRatio);


figure(100);
mesh(x, y, plaFactor(x,y),'FaceColor','interp');
xlabel('Loss');
ylabel('Loss Ratio');
zlabel('PLA Factor');
title('PLA Factor Graph')

