function [hCity,hText] = plot_cities(Top10_cities,hFig)
    
% [hCity,hText] = plot_cities(Top10_cities,hFig)
%
% Plot the major cities, according to the variable Top10_cities
%
% INPUT:
%    Top10_cities        : structure (1 x 1) with the information (Pop.,Lon,Lat) of the biggest cities in the country
%    hFig                : figure handle of target figure to plot in
%
% OUTPUT:
%    hCity               : object handle of the top10 cities plot (yellow circle)
%    hText               : object handle of the top10 cities text (name)
%


% if no figure is defined, use current figure
if nargin < 2
    hFig = gcf;
end

figure(hFig);

hold on

nCities = size(Top10_cities,1);

% loop over all cities
for i = 1:nCities
    hCity = plot(Top10_cities.Lon(i),Top10_cities.Lat(i),'o','MarkerFaceColor','y','MarkerEdgeColor','k');
    hText = text(Top10_cities.Lon(i),Top10_cities.Lat(i),['  ' Top10_cities.Name{i}]);
end

xlabel('Longitude [deg]','FontSize',12)
ylabel('Latitude [deg]','FontSize',12)
axis equal
